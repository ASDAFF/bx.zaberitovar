/**
 * @package bx.zaberitovar
 */

BX.namespace("zaberitovar");

BX.zaberitovar.updateZTStatus = function (order_id) {
    BX.showWait();
    BX.ajax.post("/bitrix/tools/bx.zaberitovar/ajax_update_status.php", {
        sessid: BX.bitrix_sessid(),
        order_id: order_id
    }, function (resp) {
        
        resp = BX.parseJSON(resp);
        
        if (resp.error) {
            alert("Update status error. Please, update the page and try again.");
        }
        
        window.location.reload();
        BX.closeWait();
    });
};