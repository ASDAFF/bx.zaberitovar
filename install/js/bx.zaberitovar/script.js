/**
 * @package bx.zaberitovar
 */

BX.namespace("zaberitovar");

BX.zaberitovar = {
    __widgetPvz: zt,
    __widgetCourier: zt_cour,
    __selectDeliveryPoint: function (result) {

        var data = result.data;

        // самовывоз
        if (data.service === "1") {

            document.querySelector("input[name=\"zt_pvz[cod]\"").value = data.cod;
            document.querySelector("input[name=\"zt_pvz[price]\"").value = data.price;
            document.querySelector("input[name=\"zt_pvz[srok]\"").value = data.srok;
            document.querySelector("input[name=\"zt_pvz[cityname]\"").value = data.cityname;
            document.querySelector("input[name=\"zt_pvz[phone]\"").value = data.phone;
            document.querySelector("input[name=\"zt_pvz[work_time]\"").value = data.work_time;
            document.querySelector("input[name=\"zt_pvz[pvz_name]\"").value = data.pvz_name;
            document.querySelector("input[name=\"zt_pvz[address]\"").value = data.address;

            BX.zaberitovar.__widgetPvz.hideOverlay();
            BX.zaberitovar.__widgetPvz.hideContainer();


        }

        // курьер
        if (data.service === "2") {

            document.querySelector("input[name=\"zt_courier[cod]\"").value = data.cod;
            document.querySelector("input[name=\"zt_courier[price]\"").value = data.price;
            document.querySelector("input[name=\"zt_courier[srok]\"").value = data.srok;
            document.querySelector("input[name=\"zt_courier[cityname]\"").value = data.cityname;
            document.querySelector("input[name=\"zt_courier[partner]\"").value = data.partner;

            BX.zaberitovar.__widgetCourier.hideOverlay();
            BX.zaberitovar.__widgetCourier.hideContainer();
        }

        if (typeof submitForm === 'function') {
            submitForm();
        } else if (typeof BX.Sale.OrderAjaxComponent.sendRequest === 'function') {
            BX.Sale.OrderAjaxComponent.sendRequest();
        }

    },
    pvzOpenWidget: function (widget_api_key) {
        BX.zaberitovar.__widgetPvz.open(BX.zaberitovar.__selectDeliveryPoint, widget_api_key, 0);
    },
    courierOpenWidget: function (widget_api_key) {
        BX.zaberitovar.__widgetCourier.open(BX.zaberitovar.__selectDeliveryPoint, widget_api_key, 0);
    }
};

var onmessage = function (e) {
    BX.zaberitovar.__selectDeliveryPoint(e);
};