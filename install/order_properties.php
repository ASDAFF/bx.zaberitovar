<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


return [
    [
        "CODE" => "ZT_DELIVERY_CODE",
        "UTIL" => "Y",
        "NAME" => Loc::getMessage("ZT_DELIVERY_CODE_PROPERTY")
    ],
    [
        "CODE" => "ZT_DELIVERY_ORDXMLID",
        "UTIL" => "Y",
        "NAME" => Loc::getMessage("ZT_DELIVERY_ORDER_XML_ID_PROPERTY")
    ],
    [
        "CODE" => "ZT_DELIVERY_DATA",
        "UTIL" => "N",
        "NAME" => Loc::getMessage("ZT_DELIVERY_DATA_PROPERTY")
    ]
];
