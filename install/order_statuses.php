<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


return [
    [
        "BX_CODE" => "ZA",
        "NAME" => Loc::getMessage("ZT_ZA_STATUS"),
        "XML_ID" => 1
    ],
    [
        "BX_CODE" => "ZB",
        "NAME" => Loc::getMessage("ZT_ZB_STATUS"),
        "XML_ID" => 2
    ],
    [
        "BX_CODE" => "ZC",
        "NAME" => Loc::getMessage("ZT_ZC_STATUS"),
        "XML_ID" => 3
    ],
    [
        "BX_CODE" => "ZD",
        "NAME" => Loc::getMessage("ZT_ZD_STATUS"),
        "XML_ID" => 5
    ],
    [
        "BX_CODE" => "ZE",
        "NAME" => Loc::getMessage("ZT_ZE_STATUS"),
        "XML_ID" => 7
    ],
    [
        "BX_CODE" => "ZF",
        "NAME" => Loc::getMessage("ZT_ZF_STATUS"),
        "XML_ID" => 10
    ],
    [
        "BX_CODE" => "ZJ",
        "NAME" => Loc::getMessage("ZT_ZJ_STATUS"),
        "XML_ID" => 11
    ],
    [
        "BX_CODE" => "ZH",
        "NAME" => Loc::getMessage("ZT_ZH_STATUS"),
        "XML_ID" => 12
    ],
    [
        "BX_CODE" => "ZI",
        "NAME" => Loc::getMessage("ZT_ZI_STATUS"),
        "XML_ID" => 30
    ],
    [
        "BX_CODE" => "ZG",
        "NAME" => Loc::getMessage("ZT_ZG_STATUS"),
        "XML_ID" => 31
    ],
    [
        "BX_CODE" => "ZK",
        "NAME" => Loc::getMessage("ZT_ZK_STATUS"),
        "XML_ID" => 32
    ],
    [
        "BX_CODE" => "ZL",
        "NAME" => Loc::getMessage("ZT_ZL_STATUS"),
        "XML_ID" => 33
    ],
    [
        "BX_CODE" => "ZM",
        "NAME" => Loc::getMessage("ZT_ZM_STATUS"),
        "XML_ID" => 34
    ],
    [
        "BX_CODE" => "ZN",
        "NAME" => Loc::getMessage("ZT_ZN_STATUS"),
        "XML_ID" => 35
    ],
    [
        "BX_CODE" => "ZO",
        "NAME" => Loc::getMessage("ZT_ZO_STATUS"),
        "XML_ID" => 39
    ],
    [
        "BX_CODE" => "ZP",
        "NAME" => Loc::getMessage("ZT_ZP_STATUS"),
        "XML_ID" => 40
    ],
    [
        "BX_CODE" => "ZQ",
        "NAME" => Loc::getMessage("ZT_ZQ_STATUS"),
        "XML_ID" => 48
    ],
    [
        "BX_CODE" => "ZR",
        "NAME" => Loc::getMessage("ZT_ZR_STATUS"),
        "XML_ID" => 49
    ],
    [
        "BX_CODE" => "ZS",
        "NAME" => Loc::getMessage("ZT_ZS_STATUS"),
        "XML_ID" => 50
    ],
    [
        "BX_CODE" => "ZT",
        "NAME" => Loc::getMessage("ZT_ZT_STATUS"),
        "XML_ID" => 51
    ],
    [
        "BX_CODE" => "ZU",
        "NAME" => Loc::getMessage("ZT_ZU_STATUS"),
        "XML_ID" => 99
    ],
    [
        "BX_CODE" => "ZY",
        "NAME" => Loc::getMessage("ZT_ZY_STATUS"),
        "XML_ID" => ""
    ]
];
