<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if (
        !$request->isAjaxRequest() ||
        !$request->isPost() ||
        $request->getPost("order_id") <= 0 ||
        !\check_bitrix_sessid()
) {

    echo \Bitrix\Main\Web\Json::encode(array('error' => true));
    die;
}

Bitrix\Main\Loader::includeModule("bx.zaberitovar");
Bitrix\Main\Loader::includeModule("sale");

$order = \Bitrix\Sale\Order::load(intval($request->getPost("order_id")));

echo \Bitrix\Main\Web\Json::encode(array('error' => !\zaberitovar\Tools::updateStatus($order)));
die;
