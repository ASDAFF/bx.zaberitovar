<?php
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

// update statuses
Bitrix\Main\Loader::includeModule("bx.zaberitovar");
Bitrix\Main\Loader::includeModule("sale");

$arFilter = [
    "DELIVERY_ID" => [
        Bitrix\Main\Config\Option::get(\zaberitovar\Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID"),
        Bitrix\Main\Config\Option::get(\zaberitovar\Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID")
    ]
];

$dbOrders = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
while ($arOrder = $dbOrders->Fetch()) {
    \zaberitovar\Tools::updateStatus(\Bitrix\Sale\Order::load($arOrder["ID"]));
}