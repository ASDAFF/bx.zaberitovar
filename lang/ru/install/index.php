<?php
$MESS["ZT_MODULE_NAME"] = "Забери товар";
$MESS["ZT_MODULE_DESCRIPTION"] = "Обработчики службы доставки \"Забери товар\"";
$MESS["ZT_PARTNER_NAME"] = "ИП Бреский Дмитрий Игоревич";
$MESS["ZT_CREATE_DIRECTORY_ERROR"] = "Неудалось создать директорию #DIR_PATH#";
$MESS["ZT_CHECK_SALE_INSTALL_ERROR"] = "Для работы модуля необходимо наличие установленного модуля Интернет-магазин";
$MESS["ZT_PVZ_DELIVERY_NAME"] = "Забери товар: самовывоз";
$MESS["ZT_DELIVERY_HAVE_ORDERS"] = "В системе присутствуют заказы, которые связаны с системой Забери товар. Для удаления модуля необходимо сначало удалить данные заказы.";
$MESS["ZT_PVZ_DELIVERY_DESC"] = "Самовывоз";
$MESS["ZT_PVZ_DELIVERY_ERROR"] = "Не удалось создать службу доставки Збери товар: Самовывоз";
$MESS["ZT_COURIER_DELIVERY_NAME"] = "Забери товар: Курьером";
$MESS["ZT_COURIER_DELIVERY_DESC"] = "Доставка курьером";
$MESS["ZT_COURIER_DELIVERY_ERROR"] = "Не удалось создать службу доставки Збери товар: Курьером";
$MESS["ZT_DELIVERY_CODE_PROPERTY"] = "Код точки доставки в системе Забери товар";
$MESS["ZT_DELIVERY_ORDER_XML_ID_PROPERTY"] = "Код заказа в системе Забери товар";
$MESS["ZT_DELIVERY_DATA_PROPERTY"] = "Информация по доставке системы Забери товар";
$MESS["ZT_ADD_PROPERTIES_ERROR"] = "Возникла ошибка при добавлении свойств заказа";
$MESS["ZT_ADD_STATUSES_ERROR"] = "Возникла ошибка при добавлении статусов заказа";
$MESS["ZT_ZA_STATUS"] = "Забери товар: Ожидается привоз заказа";
$MESS["ZT_ZB_STATUS"] = "Забери товар: На сборке";
$MESS["ZT_ZC_STATUS"] = "Забери товар: Заказ готов к выдаче";
$MESS["ZT_ZD_STATUS"] = "Забери товар: Заказ отменен покупателем";
$MESS["ZT_ZE_STATUS"] = "Забери товар: Отмена по сроку хранения";
$MESS["ZT_ZF_STATUS"] = "Забери товар: Заказ выдан";
$MESS["ZT_ZJ_STATUS"] = "Забери товар: Отменен, ожидает возврата";
$MESS["ZT_ZH_STATUS"] = "Забери товар: Заказ подготовлен на возврат";
$MESS["ZT_ZI_STATUS"] = "Забери товар: Ожидает транспортировки";
$MESS["ZT_ZG_STATUS"] = "Забери товар: Заказ передан курьеру";
$MESS["ZT_ZK_STATUS"] = "Забери товар: Заказ передан партнеру";
$MESS["ZT_ZL_STATUS"] = "Забери товар: Транспортировка";
$MESS["ZT_ZM_STATUS"] = "Забери товар: Отменен. Ожидает транспортировки";
$MESS["ZT_ZN_STATUS"] = "Забери товар: Отменен, передан курьеру";
$MESS["ZT_ZO_STATUS"] = "Забери товар: Выполнен";
$MESS["ZT_ZP_STATUS"] = "Забери товар: Ожидает забора";
$MESS["ZT_ZQ_STATUS"] = "Забери товар: Ожидает расчета";
$MESS["ZT_ZR_STATUS"] = "Забери товар: Подготовлен на возврат";
$MESS["ZT_ZS_STATUS"] = "Забери товар: Возвращен";
$MESS["ZT_ZT_STATUS"] = "Забери товар: Возвращен на склад";
$MESS["ZT_ZU_STATUS"] = "Забери товар: Утерян при транспортировке";
$MESS["ZT_ZY_STATUS"] = "Забери товар: Отгружен";
$MESS["ZT_INSTALL"] = "Установка служб доставки сервиса \"Забери товар\"";

