<?php

namespace zaberitovar;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class EventsHandlers {

    /**
     * @param array $arResult
     * @return boolean
     */
    public static function onSaleComponentOrderOneStepDelivery(&$arResult) {


        if ($arResult["DELIVERY"][Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID")]["CHECKED"] === "Y") {

            $arData = Tools::processingPvzRequest($arResult);

            $arResult["DELIVERY"][Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID")]["DESCRIPTION"] .= "<br><br>" . Tools::pvzDeliveryDescription($arData);
        } elseif ($arResult["DELIVERY"][Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID")]["CHECKED"] === "Y") {

            $arData = Tools::processingCourierRequest($arResult);

            $arResult["DELIVERY"][Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID")]["DESCRIPTION"] .= "<br><br>" . Tools::courierDeliveryDescription($arData);
        }

        return true;
    }

    /**
     * @param \Bitrix\Sale\Order $order
     * @return boolean
     */
    public static function onSaleStatusOrderChange(\Bitrix\Sale\Order $order) {

        $status_id = $order->getField("STATUS_ID");

        if ($status_id === "ZY" && Tools::needCallRequest($order)) {

            $result = Tools::sendRequest(Tools::getCreateOrderRequestBody($order));

            // parse result
            if ($result) {
                $params = $result->params;
                if ($params && $params->status == 0) {
                    $zt_order = $params->orders->order;
                    if ($zt_order->status == "ok") {
                        $uid = $zt_order->uid;
                        Tools::getOrderPropItem($order, "ZT_DELIVERY_ORDXMLID")->setField("VALUE", $uid[0]);
                        $order->getPropertyCollection()->save();
                    }
                }
            }
        }

        return true;
    }

    public static function onSaleComponentOrderProperties(&$arUserResult, $request) {

        $optionsPropData = unserialize(Option::get(Tools::MODULE_ID, "ZT_ORDER_PROPERTIES"));

        if (isset($optionsPropData[$arUserResult["PERSON_TYPE_ID"]])) {
            $orderRequest = $request->getPost("order");
            $arData = Tools::getZTDeliveryDataById($orderRequest["DELIVERY_ID"]);
            foreach (\array_keys($arUserResult["ORDER_PROP"]) as $prop_id) {
                if ($optionsPropData[$arUserResult["PERSON_TYPE_ID"]][$prop_id] == "ZT_DELIVERY_DATA" && strlen($arData["desc"])) {
                    $arUserResult["ORDER_PROP"][$prop_id] = $arData["desc"];
                }
            }
        }

        return true;
    }

    /**
     * @param \Bitrix\Main\Event $order
     * @return boolean
     */
    public static function onSaleOrderBeforeSaved(\Bitrix\Sale\Order $order) {

        $deliveriesId = $order->getDeliveryIdList();

        $is_zt_pvz_delivery = in_array(Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID"), $deliveriesId);
        $is_zt_courier_delivery = in_array(Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID"), $deliveriesId);

        if (!$is_zt_pvz_delivery && !$is_zt_courier_delivery) {
            return true;
        }

        if ($is_zt_pvz_delivery) {
            $arDeliveryData = Tools::getDeliveryData("zt_pvz", "ZT_PVZ_DELIVERY_DATA");
        } elseif ($is_zt_courier_delivery) {
            $arDeliveryData = Tools::getDeliveryData("zt_courier", "ZT_COURIER_DELIVERY_DATA");
        }

        $propertyCollection = $order->getPropertyCollection();

        foreach ($propertyCollection as $property) {

            switch ($property->getField("CODE")) {

                case "ZT_DELIVERY_CODE":
                    $property->setField("VALUE", $arDeliveryData["cod"]);
                    break;
                case "ZT_DELIVERY_DATA":
                    $property->setField("VALUE", $arDeliveryData["desc"]);
                    break;
            }
        }


        return true;
    }

    /**
     * @param array $arResult
     * @return boolean
     */
    public static function onSaleComponentOrderJsData(&$arResult) {

        Tools::loadJS();

        if ($arResult["JS_DATA"]["TOTAL"]["DELIVERY_PRICE"] <= 0) {
            $delivery_id = 0;
            if ($arResult["DELIVERY"][Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID")]["CHECKED"] === "Y") {

                $delivery_id = Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID");
            } elseif ($arResult["DELIVERY"][Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID")]["CHECKED"] === "Y") {

                $delivery_id = Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID");
            }

            if ($delivery_id) {
                $arResult["JS_DATA"]["TOTAL"]["DELIVERY_PRICE"] = $arResult["DELIVERY"][$delivery_id]["PRICE"];
                $arResult["JS_DATA"]["TOTAL"]["DELIVERY_PRICE_FORMATED"] = $arResult["DELIVERY"][$delivery_id]["PRICE_FORMATED"];

                $total_price = $arResult["DELIVERY"][$delivery_id]["PRICE"] + $arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE"];

                $arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE"] = $total_price;
                $arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE_FORMATED"] = Tools::currencyFormat($total_price);
            }
        }

        return true;
    }

    /**
     * @global \CMain $APPLICATION
     * @param array $items
     * @return boolean
     */
    public static function onAdminContextMenuShow(&$items) {

        global $APPLICATION;

        $MODULE_ID = Tools::MODULE_ID;

        $pages = [
            '/bitrix/admin/sale_order_edit.php',
            '/bitrix/admin/sale_order_view.php'
        ];

        if (
                $_SERVER["REQUEST_METHOD"] === "GET" &&
                $_REQUEST["ID"] > 0 &&
                in_array($APPLICATION->GetCurPage(), $pages)
        ) {

            $code = Tools::getOrderPropItem(\Bitrix\Sale\Order::load($_REQUEST["ID"]), "ZT_DELIVERY_ORDXMLID")->getField("VALUE");
            if (!empty($code)) {
                $APPLICATION->AddHeadString("<script src=\"/bitrix/js/{$MODULE_ID}/admin_script.js\"></script>");
                $items[] = Tools::getBtnItemArray(intval($_REQUEST["ID"]));
            }
        }

        return true;
    }

    /**
     * 
     * @param \Bitrix\Main\EventResult $result
     * @param \Bitrix\Sale\Shipment $shipment
     * @param int $delivery_id
     * @return int
     */
    public static function onSaleDeliveryServiceCalculate($result, $shipment, $delivery_id) {

        $arData = Tools::getZTDeliveryDataById($delivery_id);
        if (!empty($arData)) {

            if ($arData["price"] > 0 && $result->getDe) {
                $result->setDeliveryPrice($arData["price"]);
            }
            if ($arData["srok"]) {
                $result->setPeriodFrom($arData["srok"]);
                $result->setPeriodTo($arData["srok"]);
            }
        }

        return \Bitrix\Main\EventResult::SUCCESS;
    }

}
