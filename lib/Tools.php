<?php

namespace zaberitovar;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Tools {

    const MODULE_ID = "bx.zaberitovar";

    /**
     * @param array $arData
     * @return string
     */
    public static function pvzDeliveryDescription(array $arData) {

        ob_start();
        require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/views/pvz.php";
        return ob_get_clean();
    }

    /**
     * @param array $arData
     * @return string
     */
    public static function courierDeliveryDescription(array $arData) {

        ob_start();
        require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/views/courier.php";
        return ob_get_clean();
    }

    /**
     * @global \CMain $APPLICATION
     */
    public static function loadJS() {
        global $APPLICATION;

        $MODULE_ID = self::MODULE_ID;

        $APPLICATION->AddHeadString("<script src=\"//api.zaberi-tovar.ru/widget/pvz.js\"></script>");
        $APPLICATION->AddHeadString("<script src=\"//api.zaberi-tovar.ru/widget-cour/pvz.js\"></script>");
        $APPLICATION->AddHeadString("<script src=\"/bitrix/js/{$MODULE_ID}/script.js\"></script>");
    }

    /**
     * @param array $arResult
     * @return array
     */
    public static function processingPvzRequest(&$arResult) {

        return self::processingDeliveryData("ZT_PVZ_DELIVERY_ID", "zt_pvz", "ZT_PVZ_DELIVERY_DATA", "ZT_PVZ_DELIVERY_POINT_CHOOSE_ERROR", $arResult);
    }

    /**
     * @param array $arResult
     * @return type
     */
    public static function processingCourierRequest(&$arResult) {


        return self::processingDeliveryData("ZT_COURIER_DELIVERY_ID", "zt_courier", "ZT_COURIER_DELIVERY_DATA", "ZT_COURIER_DELIVERY_POINT_CHOOSE_ERROR", $arResult);
    }

    /**
     * @param mixed $var
     */
    public function __dump($var) {

        ob_start();

        echo "<pre>";
        print_r($var);
        echo "</pre>";

        \file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/__dump.txt", ob_get_clean());
    }

    /**
     * @param string $request_key
     * @param string $session_key
     * @return array
     */
    public static function getDeliveryData($request_key, $session_key) {

        $arData = ["widget_api_key" => \Bitrix\Main\Config\Option::get(self::MODULE_ID, "ZT_WIDGET_API_KEY")];

        if (isset($_REQUEST["order"][$request_key]) && is_array($_REQUEST["order"][$request_key]) && !empty($_REQUEST["order"][$request_key])) {
            $arData = \array_merge($_REQUEST["order"][$request_key], $arData);

            $_SESSION[$session_key] = $_REQUEST["order"][$request_key];
        } elseif (isset($_SESSION[$session_key]) && is_array($_SESSION[$session_key]) && !empty($_SESSION[$session_key])) {
            $arData = \array_merge($_SESSION[$session_key], $arData);
        }

        $arDesc = [];
        if (strlen($arData["cityname"])) {
            $arDesc[] = \htmlspecialcharsbx($arData["cityname"]);
        }
        if (strlen($arData["partner"])) {
            $arDesc[] = \htmlspecialcharsbx($arData["partner"]);
        }
        if (strlen($arData["address"])) {
            $arDesc[] = \htmlspecialcharsbx($arData["address"]);
        }
        if (strlen($arData["phone"])) {
            $arDesc[] = \htmlspecialcharsbx($arData["phone"]);
        }
        $arData["desc"] = implode(", ", $arDesc);
        return $arData;
    }

    /**
     * 
     * @param int $delivery_id
     */
    public static function getZTDeliveryDataById($delivery_id) {

        if ($delivery_id == Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID")) {
            return self::getDeliveryData("zt_pvz", "ZT_PVZ_DELIVERY_DATA");
        } elseif ($delivery_id == Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID")) {
            return self::getDeliveryData("zt_courier", "ZT_COURIER_DELIVERY_DATA");
        }
        return [];
    }

    /**
     * 
     * @param string $option_delivery_key
     * @param string $request_key
     * @param string $session_key
     * @param string $lang_key
     * @param array $arResult
     * @return array
     */
    public static function processingDeliveryData($option_delivery_key, $request_key, $session_key, $lang_key, &$arResult) {

        $delivery_id = Option::get(self::MODULE_ID, $option_delivery_key);

        $arData = self::getDeliveryData($request_key, $session_key);

        if (key_exists("cod", $arData)) {
            if (empty($arData["cod"])) {
                $arResult["ERROR_SORTED"]["DELIVERY"][] = Loc::getMessage($lang_key);
            } else {
                $arResult["DELIVERY"][$delivery_id]["PRICE"] = $arData["price"];
                $arResult["DELIVERY"][$delivery_id]["PRICE_FORMATED"] = self::currencyFormat($arData["price"]);
            }
        }

        return $arData;
    }

    /**
     * @param string $price
     * @return string
     */
    public static function currencyFormat($price) {

        \Bitrix\Main\Loader::includeModule("currency");
        return \CCurrencyLang::CurrencyFormat($price, "RUB");
    }

    /**
     * @param \Bitrix\Sale\Order $order
     * @param array $code
     * @return mixed
     */
    public function getOrderPropItem($order, $code) {

        $personTypesId = $order->getPersonTypeId();

        $optionsProps = \unserialize(Option::get(Tools::MODULE_ID, "ZT_ORDER_PROPERTIES"));

        $props = $optionsProps[$personTypesId];
        foreach ($props as $propId => $propCode) {

            if ($propCode === $code) {

                return $order->getPropertyCollection()->getItemByOrderPropertyId($propId);
            }
        }

        return null;
    }

    /**
     * @param \Bitrix\Sale\Order $order
     * @return boolean
     */
    public function needCallRequest($order) {

        $order_xml_id = self::getOrderPropItem($order, "ZT_DELIVERY_ORDXMLID")->getField("VALUE");

        $delivery_code = self::getOrderPropItem($order, "ZT_DELIVERY_CODE")->getField("VALUE");

        return !empty($delivery_code) && empty($order_xml_id);
    }

    /**
     * @param \Bitrix\Sale\Order $order
     * @return string
     */
    public function getCreateOrderRequestBody($order) {

        $service_id = self::getDeliveryServiceIdByOrder($order);

        $order_id = $order->getId();

        $price = $order->getPrice();

        $propertyCollection = $order->getPropertyCollection();

        $fio = "";
        $fio_prop = $propertyCollection->getPayerName();
        if ($fio_prop) {
            $fio = $fio_prop->getField("VALUE");
        } else {
            $fio_prop = $propertyCollection->getProfileName();
            if ($fio_prop) {
                $fio = $fio_prop->getField("VALUE");
            }
        }

        if (!strlen($fio)) {
            $user = \CUser::GetByID($order->getUserId())->Fetch();
            if (isset($user["ID"])) {
                $fio = implode(" ", [
                    $user["NAME"],
                    $user["SECOND_NAME"],
                    $user["LAST_NAME"]
                ]);
                if (!$fio) {
                    $fio = $user["LOGIN"];
                }
            }
        }
        $phone = "";
        $phone_prop = $propertyCollection->getPhone();
        if ($phone_prop) {
            $phone = $phone_prop->getField("VALUE");
        }
        
        $address = self::getOrderPropItem($order, "ZT_DELIVERY_DATA")->getField("VALUE");
        
        $comment = $order->getField('USER_DESCRIPTION');
        $delivery_code = self::getOrderPropItem($order, "ZT_DELIVERY_CODE")->getField("VALUE");
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <methodCall><methodName>add_new_order</methodName>" . self::getCommonXmlResuestString() . "
                    <params>
                        <orders>
                            <item>
                                <order_id>$order_id</order_id>
                                <int_number>$order_id</int_number>
                                <service>$service_id</service>
                                <order_amount>$price</order_amount>
                                <d_price>$price</d_price>
                                <fio>$fio</fio>
                                <phone>$phone</phone>".($service_id != 1 ? "<address>$address</address>" : "")."
                                <comment>$comment</comment>
                                <final_pv>$delivery_code</final_pv>
                                <weight>1</weight>
                                <goods></goods>
                            </item>
                        </orders>
                    </params>
                </methodCall>";
    }

    /**
     * @return string
     */
    public static function getCommonXmlResuestString() {
        $login = Option::get(self::MODULE_ID, "ZT_LOGIN");
        $client_api_id = Option::get(self::MODULE_ID, "ZT_CLIENT_API_KEY");
        return "<client_name>$login</client_name><client_api_id>$client_api_id</client_api_id>";
    }

    /**
     * @param \Bitrix\Sale\Order $order
     * @return string
     */
    public static function getUpdateStatusRequestBody($order) {

        $uid = self::getOrderPropItem($order, "ZT_DELIVERY_ORDXMLID")->getField("VALUE");
        $order_id = $order->getId();
        if (empty($uid)) {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <methodCall><methodName>get_orders_by_order_id</methodName>" . self::getCommonXmlResuestString() . "
                <params>
                      <orders>
                                <order_id>$order_id</order_id>
                          </orders>
                </params>
                </methodCall>
            ";
        }
        
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                    <methodCall><methodName>get_orders_by_id</methodName>" . self::getCommonXmlResuestString() . "
                    <params>
                          <orders>
                                    <id>$uid</id>
                              </orders>
                    </params>
                    </methodCall>
                ";
    }

    /**
     * @param string $body
     * @return \SimpleXMLElement
     */
    public static function sendRequest($body) {
        $httpClient = new \Bitrix\Main\Web\HttpClient;
        $httpClient->setTimeout(20);
        $result = $httpClient->post("http://lc.zaberi-tovar.ru/api/", [
            "xml" => $body
        ]);
        
        return \simplexml_load_string($result);
    }

    /**
     * @param int $delivery_ids
     * @return int
     */
    public static function getDeliveryServiceIdByIds($delivery_ids) {
        $service_id = 0;
        if (in_array(Option::get(Tools::MODULE_ID, "ZT_PVZ_DELIVERY_ID"), $delivery_ids)) {
            $service_id = 1;
        } elseif (in_array(Option::get(Tools::MODULE_ID, "ZT_COURIER_DELIVERY_ID"), $delivery_ids)) {
            $service_id = 2;
        }
        return $service_id;
    }

    /**
     * @param \Bitrix\Sale\Order $order $order
     * @return int
     */
    public static function getDeliveryServiceIdByOrder($order) {
        return self::getDeliveryServiceIdByIds($order->getDeliveryIdList());
    }

    /**
     * @param int $order_id
     * @return array
     */
    public static function getBtnItemArray($order_id) {
        return [
            "TEXT" => Loc::getMessage("ZT_DELIVERY_ADMIN_BTN_TITLE"),
            "TITLE" => Loc::getMessage("ZT_DELIVERY_ADMIN_BTN_TITLE"),
            "LINK" => "javascript: BX.zaberitovar.updateZTStatus(" . $order_id . ")"
        ];
    }

    /**
     * @param \Bitrix\Sale\Order $order
     * @return boolean
     */
    public static function updateStatus($order) {
        
        $result = self::sendRequest(self::getUpdateStatusRequestBody($order));

        $orders = $result->params->orders;

        $statusesOptions = \unserialize(Option::get(Tools::MODULE_ID, "ZT_ORDER_STATUSES"));

        if ($orders->item) {
            $status = $orders->item[0]->status->__toString();
            if ($status > 0) {

                if ($statusesOptions[$status]) {

                    $order->setField("STATUS_ID", $statusesOptions[$status]);
                    $order->save();
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getStatusesCodeList() {
        $codes = [];
        $statusesOptions = \unserialize(Option::get(self::MODULE_ID, "ZT_ORDER_STATUSES"));
        foreach ($statusesOptions as $code) {
            $codes[] = $code;
        }
        return $codes;
    }

}
