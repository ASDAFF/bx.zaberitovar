<?php
if (!$USER->isAdmin())
    return;

$mid = "bx.zaberitovar";

\Bitrix\Main\Loader::includeModule($mid);

global $APPLICATION;

function renderOptions($arOptions, $mid) {

    foreach ($arOptions as $name => $arValues) {

        $cur_opt_val = htmlspecialcharsbx(Bitrix\Main\Config\Option::get($mid, $name));
        $name = htmlspecialcharsbx($name);

        $options .= '<tr>';
        $options .= '<td width="40%">';
        $options .= '<label for="' . $name . '">' . $arValues['DESC'] . ':</label>';
        $options .= '</td>';
        $options .= '<td width="60%">';
        if ($arValues['TYPE'] == 'select') {

            $options .= '<select id="' . $name . '" name="' . $name . '">';
            foreach ($arValues['VALUES'] as $key => $value) {
                $options .= '<option ' . ($cur_opt_val == $key ? 'selected' : '') . ' value="' . $key . '">' . $value . '</option>';
            }
            $options .= '</select>';
        } elseif ($arValues['TYPE'] == 'text' || $arValues['TYPE'] == 'password') {

            $options .= '<input type="'.$arValues['TYPE'].'" name="' . $name . '" value="' . $cur_opt_val . '">';
        } elseif ($arValues['TYPE'] == 'checkbox') {
            
            $options .= '<input type="hidden" name="' . $name . '" value="N">';
            $options .= '<input type="checkbox" '.($cur_opt_val === "Y" ? "checked" : "").' name="' . $name . '" value="Y">';
        }
        $options .= '</td>';
        $options .= '</tr>';
    }
    echo $options;
}

$main_options = array(
    'TOTAL_OPTS' => array(  
        'ZT_LOGIN' => array('DESC' => "login", "VALUES" => Bitrix\Main\Config\Option::get($mid, "ZT_LOGIN"), 'TYPE' => 'text'),
        'ZT_PASSWORD' => array('DESC' => "password", "VALUES" => Bitrix\Main\Config\Option::get($mid, "ZT_PASSWORD"), 'TYPE' => 'password'),
        'ZT_CLIENT_API_KEY' => array('DESC' => "Client api key", "VALUES" => Bitrix\Main\Config\Option::get($mid, "ZT_CLIENT_API_KEY"), 'TYPE' => 'text'),
        'ZT_WIDGET_API_KEY' => array('DESC' => "widget api key", "VALUES" => Bitrix\Main\Config\Option::get($mid, "ZT_WIDGET_API_KEY"), 'TYPE' => 'text')
    )
);

$tabs = array(
    
    array(
        "DIV" => "edit4",
        "TAB" => "Common options",
        "ICON" => "",
        "TITLE" => "Common options"
    )
);

$o_tab = new CAdminTabControl("TravelsoftTabControl", $tabs);
if ($REQUEST_METHOD == "POST" && strlen($save . $reset) > 0 && check_bitrix_sessid()) {

    if (strlen($reset) > 0) {
        foreach ($main_options as $arBlockOption) {

            foreach (\array_keys($arBlockOption) as $name) {
                \Bitrix\Main\Config\Option::delete($mid, array('name' => $name));
            }
        }
    } else {
        foreach ($main_options as $arBlockOption) {

            foreach ($arBlockOption as $name => $arValues) {
                if (isset($_REQUEST[$name])) {
                    \Bitrix\Main\Config\Option::set($mid, $name, $_REQUEST[$name]);
                }
            }
        }
    }

    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&" . $o_tab->ActiveTabParam());
}
$o_tab->Begin();
?>

<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
<?
foreach ($main_options as $arOption) {
    $o_tab->BeginNextTab();
    renderOptions($arOption, $mid);
}
$o_tab->Buttons();
?>
    <input type="submit" name="save" value="Save" title="Save" class="adm-btn-save">
    <input type="submit" name="reset" title="Reset" OnClick="return confirm('Reset ?')" value="Reset">
<?= bitrix_sessid_post(); ?>
    <? $o_tab->End(); ?>
</form>
