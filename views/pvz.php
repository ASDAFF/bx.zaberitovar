<?php

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="zt-pvz-delivery-description-block">
    <input name="zt_pvz[cod]" value="<?= \htmlspecialcharsbx($arData["cod"]) ?>" type="hidden">
    <input name="zt_pvz[price]" value="<?= \htmlspecialcharsbx($arData["price"]) ?>" type="hidden">
    <input name="zt_pvz[srok]" value="<?= \htmlspecialcharsbx($arData["srok"]) ?>" type="hidden">
    <input name="zt_pvz[cityname]" value="<?= \htmlspecialcharsbx($arData["cityname"]) ?>" type="hidden">
    <input name="zt_pvz[phone]" value="<?= \htmlspecialcharsbx($arData["phone"]) ?>" type="hidden">
    <input name="zt_pvz[work_time]" value="<?= \htmlspecialcharsbx($arData["work_time"]) ?>" type="hidden">
    <input name="zt_pvz[pvz_name]" value="<?= \htmlspecialcharsbx($arData["pvz_name"]) ?>" type="hidden">
    <input name="zt_pvz[address]" value="<?= \htmlspecialcharsbx($arData["address"]) ?>" type="hidden">

    <?php
    $title = Loc::getMessage("ZT_PVZ_OPEN_WIDGET_SET_POINT_TITLE");
    if (!empty($arData["cod"])):
        $title = Loc::getMessage("ZT_PVZ_OPEN_WIDGET_CHANGE_POINT_TITLE");
        echo Loc::getMessage("ZT_PVZ_ADDRESS", ["#ADDRESS#" => $arData["desc"]]);
        ?>
        <br>
        <br>
    <? endif ?>
    <a href="javascript:void(0)" onclick="BX.zaberitovar.pvzOpenWidget('<?= \htmlspecialcharsbx($arData["widget_api_key"]) ?>')"><?= $title ?></a>
</div>
